FROM golang:1.12 as builder

ADD . /go/src/avias
WORKDIR /go/src/avias

RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -a -installsuffix netgo -o /avias main.go

FROM alpine:3.9 as prod

RUN apk update \
        && apk upgrade \
        && apk add --no-cache \
        ca-certificates \
        && update-ca-certificates 2>/dev/null || true

COPY --from=builder /avias /avias
COPY --from=builder /go/src/avias/example.yaml /config.yaml

CMD /avias server --config=config.yaml