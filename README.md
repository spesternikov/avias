# AVIAS

Test task

## Requirements

* Go ver >=1.12

## Build

```sh
$ make build

```

or 

```sh
$ GO111MODULE=on go build -o ./bin/avias ./main.go

```

## Run

```sh
$ avias server --config=example.yaml

```

## Docker

```sh
$ docker build -t registry.gitlab.com/spesternikov/avias .
$ docker-compose up -d

```

## Configuration

* **logger** - logger configuration
    * **debug** or env `LOGGER_DEBUG` (bool, optional, default: false) - debug mode
    * **level** or env `LOGGER_LEVEL` (string, optional, default: info) - level
    * **output** or env `LOGGER_OUTPUT` (list[string], optional) - output paths
* **cache** - cache configuration
    * **common** - common cache configuration
        - **expiration** or env `CACHE_COMMON_EXPIRATION` (time.Duration, optional, default: 0) -
         expiration (0 - no expiration)
    * **type** or env `CACHE_TYPE` (string, required) - type (redis, memory)
    * **redis** - redis cache configuration
        * **host** or env `CACHE_REDIS_HOST` (string, optional, default: localhost) - host
        * **port** or env `CACHE_REDIS_PORT` (int, optional, default: 6379) - port
        * **password** or env `CACHE_REDIS_PASSWORD` (string, optional) - password
        * **db** or env `CACHE_REDIS_DB` (int, optional, default: 0) - db
        * **read-timeout** or env `CACHE_REDIS_READ-TIMEOUT` (time.Duration, optional, default: 3s) - read timeout
        * **write-timeout** or env `CACHE_REDIS_WRITE-TIMEOUT` (time.Duration, optional, default: 3s) - write timeout
        * **pool-size** or env `CACHE_REDIS_POOL-SIZE` (int, optional, default: 10 per CPU) - connection pool size
        * **pool-timeout** or env `CACHE_REDIS_POOL-TIMEOUT` (time.Duration, optional, default: read-timeout + 1s) - 
        waits for connection are busy before returning an error
        * **idle-timeout** or env `CACHE_REDIS_IDLE-TIMEOUT` (time.Duration, optional, default: 5m) - idle timeout 
        (-1 - disable)
        * **idle-check-frequency** or env `CACHE_REDIS_IDLE-CHECK-FREQUENCY` (time.Duration, optional, default: 1m) - 
        idle check frequency (-1 - disable)
    * **memory** - in memory cache configuration
        * **cleanup-interval** or env `CACHE_MEMORY_CLEANUP-INTERVAL` (time.Duration, optional, default: 0) - cleanup interval aka gc (0 - no expiration)
* **processor** - requests processor configuration
    * **end-point** or env `PROCESSOR_END-POINT` (string, required) - end point for requests (example: https://places.aviasales.ru)
    * **rate-limit** or env `PROCESSOR_RATE-LIMIT` (int, optional, default: 0) - rate limit for parallels requests (0 - no rate limit)
    * **timeout** or env `PROCESSOR_TIMEOUT` (time.Duration, required) - request performing timeout
* **server** - http server configuration
    * **mode** or env `SERVER_MODE` (string, optional, default: release) - server mode (debug, test, release)
    * **host** or env `SERVER_HOST` (string, optional, default: localhost) - host
    * **port** or env `SERVER_PORT` (int, optional, default: 1234) - port
    * **read-timeout** or env `SERVER_READ-TIMEOUT` (time.Duration, optional, default: 20s) - read timeout
    * **write-timeout** or env `SERVER_WRITE-TIMEOUT` (time.Duration, optional, default: 20s) - write timeout
    * **idle-timeout** or env `SERVER_IDLE-TIMEOUT` (time.Duration, optional, default: 30s) - idle timeout
    * **request-timeout** or env `SERVER_REQUEST-TIMEOUT` (time.Duration, optional, default: 3s) - request performing
    timeout
    * **shutdown-timeout** or env `SERVER_SHUTDOWN-TIMEOUT` (time.Duration, optional, default: 5s) - shutdown timeout
    

    
        
        

