package server

import (
	"context"
	"net"
	"net/http"
	"strconv"
	"time"

	"avias/internal/pkg/cache"
	"avias/internal/pkg/processor"
	"avias/pkg/dto"

	"github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type Server struct {
	cache     cache.Cache
	processor *processor.Processor
	server    *http.Server
	logger    *zap.Logger

	requestTimeout  time.Duration
	shutdownTimeout time.Duration
}

func New(config *Config, cache cache.Cache, processor *processor.Processor, logger *zap.Logger) *Server {
	c := config.withDefaults()

	s := &Server{
		cache:           cache,
		processor:       processor,
		requestTimeout:  c.RequestTimeout,
		shutdownTimeout: c.ShutdownTimeout,
		logger:          logger.With(zap.String("service", "server")),
	}

	r := gin.New()

	gin.SetMode(config.Mode)

	ginLogger := s.logger.With(zap.String("server", "gin"))

	r.Use(ginzap.Ginzap(ginLogger, time.RFC3339, true))

	r.Use(ginzap.RecoveryWithZap(ginLogger, true))

	r.GET(c.Route, s.servePlaces)

	server := &http.Server{
		Addr:         net.JoinHostPort(c.Host, strconv.Itoa(c.Port)),
		ReadTimeout:  c.ReadTimeout,
		WriteTimeout: c.WriteTimeout,
		IdleTimeout:  c.IdleTimeout,
		Handler:      r,
	}

	s.server = server

	return s
}

func (s *Server) Serve() error {
	s.logger.Debug("starting http server")

	err := s.server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		return errors.Wrap(err, " failed to listen and serve")
	}

	return nil
}

func (s *Server) Shutdown() error {
	s.logger.Debug("stopping http server")

	ctx, cancel := context.WithTimeout(context.Background(), s.shutdownTimeout)
	defer cancel()

	err := s.server.Shutdown(ctx)
	if err != nil {
		return errors.Wrap(err, "shutdown server failed")
	}

	return nil
}

func (s *Server) servePlaces(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), s.requestTimeout)
	defer cancel()

	term := c.Query("term")
	if len(term) == 0 {
		c.JSON(http.StatusBadRequest, &dto.Error{Error: "term contains bad characters"})
		return
	}

	locale := c.Query("locale")

	key := term + locale

	data, ok, err := s.cache.Get(ctx, key)
	if err != nil && errors.Cause(err) != context.DeadlineExceeded {
		s.logger.Error("get from cache failed", zap.String("key", key))

	} else if errors.Cause(err) == context.DeadlineExceeded {
		c.JSON(http.StatusGatewayTimeout, &dto.Error{Error: err.Error()})
		return
	}

	if ok {
		c.Data(http.StatusOK, "application/json", data)
		return
	}

	res, err := s.processor.Do(ctx, key, c.Request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &dto.Error{Error: err.Error()})
		return
	}

	select {
	case <-ctx.Done():
		c.JSON(http.StatusGatewayTimeout, &dto.Error{Error: ctx.Err().Error()})
	case response := <-res:
		if response.Error != nil {
			c.JSON(response.Code, response.Error)
			return
		}
		c.Data(http.StatusOK, "application/json", response.Data)
	}

}
