package server

import (
	"time"

	"github.com/gin-gonic/gin"
)

const (
	DefaultHost            = "localhost"
	DefaultPort            = 1234
	DefaultRoute           = "/v2/places.json"
	DefaultReadTimeout     = 20 * time.Second
	DefaultWriteTimeout    = 20 * time.Second
	DefaultIdleTimeout     = 30 * time.Second
	DefaultRequestTimeout  = 3 * time.Second
	DefaultShutdownTimeout = 5 * time.Second
)

type Config struct {
	Mode            string        `mapstructure:"mode"`
	Host            string        `mapstructure:"host"`
	Port            int           `mapstructure:"port"`
	Route           string        `mapstructure:"route"`
	ReadTimeout     time.Duration `mapstructure:"read-timeout"`
	WriteTimeout    time.Duration `mapstructure:"write-timeout"`
	IdleTimeout     time.Duration `mapstructure:"idle-timeout"`
	RequestTimeout  time.Duration `mapstructure:"request-timeout"`
	ShutdownTimeout time.Duration `mapstructure:"shutdown-timeout"`
}

func (c *Config) withDefaults() (conf Config) {
	if c != nil {
		conf = *c
	}

	if len(conf.Host) == 0 {
		conf.Host = DefaultHost
	}

	if conf.Port == 0 {
		conf.Port = DefaultPort
	}

	if len(conf.Route) == 0 {
		conf.Route = DefaultRoute
	}

	if conf.ReadTimeout == 0 {
		conf.ReadTimeout = DefaultReadTimeout
	}

	if conf.WriteTimeout == 0 {
		conf.WriteTimeout = DefaultWriteTimeout
	}

	if conf.IdleTimeout == 0 {
		conf.WriteTimeout = DefaultIdleTimeout
	}

	if conf.RequestTimeout == 0 {
		conf.RequestTimeout = DefaultRequestTimeout
	}

	if conf.ShutdownTimeout == 0 {
		conf.ShutdownTimeout = DefaultShutdownTimeout
	}

	if len(conf.Mode) == 0 {
		conf.Mode = gin.ReleaseMode
	}

	return
}
