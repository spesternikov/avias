package logger

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	DefaultLevel = InfoLevel
)

type Config struct {
	Debug  bool     `mapstructure:"debug"`
	Level  string   `mapstructure:"level"`
	Output []string `mapstructure:"output"`
}

func (c *Config) convert() *zap.Config {
	var config zap.Config
	if c.Debug {
		config = zap.NewDevelopmentConfig()
	} else {
		config = zap.NewProductionConfig()
	}

	config.Level = c.getLevel()

	if len(c.Output) != 0 {
		config.OutputPaths = c.Output
	}

	return &config
}

func (c *Config) getLevel() zap.AtomicLevel {
	switch c.Level {
	case DebugLevel:
		return zap.NewAtomicLevelAt(zapcore.DebugLevel)
	case ErrorLevel:
		return zap.NewAtomicLevelAt(zapcore.ErrorLevel)
	case InfoLevel:
		return zap.NewAtomicLevelAt(zapcore.InfoLevel)
	case WarnLevel:
		return zap.NewAtomicLevelAt(zapcore.WarnLevel)
	default:
		panic("unknown log level")
	}
}

func (c *Config) withDefaults() (config Config) {
	if c != nil {
		config = *c
	}

	if len(config.Level) == 0 {
		config.Level = DefaultLevel
	}

	return
}
