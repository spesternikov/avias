package logger

import (
	"go.uber.org/zap"
)

const (
	DebugLevel = "debug"
	ErrorLevel = "error"
	InfoLevel  = "info"
	WarnLevel  = "warn"
)

func New(cfg *Config) (*zap.Logger, error) {
	config := cfg.withDefaults()

	return config.convert().Build()
}
