package processor

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"sync"

	"avias/internal/pkg/cache"
	"avias/pkg/dto"

	"github.com/pkg/errors"
	"go.uber.org/zap"
	"golang.org/x/sync/semaphore"
)

var ErrClosed = errors.New("processor closed")

type Response struct {
	Data  []byte
	Error *dto.Error
	Code  int
}

type responder struct {
	ctx  context.Context
	res  chan *Response
	addr string
}

type Processor struct {
	cancel     context.CancelFunc
	ctx        context.Context
	cache      cache.Cache
	config     *Config
	client     *http.Client
	sema       *semaphore.Weighted
	logger     *zap.Logger
	responders map[string][]*responder
	lock       sync.Mutex
	once       sync.Once
}

func responseError(err error, code int) *Response {
	dtoErr := &dto.Error{Error: err.Error()}
	return &Response{Code: code, Error: dtoErr}
}

func New(config *Config, cache cache.Cache, logger *zap.Logger) (*Processor, error) {
	var sema *semaphore.Weighted
	if config.RateLimit != 0 {
		sema = semaphore.NewWeighted(int64(config.RateLimit))
	}

	ctx, cancel := context.WithCancel(context.Background())

	return &Processor{
		cancel:     cancel,
		ctx:        ctx,
		cache:      cache,
		config:     config,
		client:     &http.Client{},
		sema:       sema,
		logger:     logger.With(zap.String("service", "processor")),
		responders: make(map[string][]*responder),
	}, nil
}

func (p *Processor) Close() error {
	err := ErrClosed
	p.once.Do(func() {
		p.cancel()
		err = nil
	})

	return err
}

func (p *Processor) Do(ctx context.Context, key string, req *http.Request) (<-chan *Response, error) {
	if p.isClosed() {
		return nil, ErrClosed
	}

	req, err := http.NewRequest(req.Method, p.config.EndPoint+req.RequestURI, nil)
	if err != nil {
		return nil, errors.Wrap(err, "create http request failed")
	}

	resp := &responder{
		ctx: ctx,
		res: make(chan *Response),
	}

	p.lock.Lock()
	_, ok := p.responders[key]
	p.responders[key] = append(p.responders[key], resp)
	p.lock.Unlock()

	if !ok {
		go p.do(key, req)
	}

	return resp.res, nil
}

func (p *Processor) do(key string, req *http.Request) {
	if p.config.RateLimit != 0 {
		err := p.sema.Acquire(p.ctx, 1)
		if err != nil {
			return
		}
		defer p.sema.Release(1)
	}

	p.process(key, req)
}

func (p *Processor) process(key string, req *http.Request) {
	ctx, cancel := context.WithTimeout(p.ctx, p.config.Timeout)
	defer cancel()

	req.WithContext(ctx)

	res, err := p.client.Do(req)
	if err != nil {
		p.logger.Error("do response failed", zap.String("key", key), zap.Error(err))
		p.sendResponse(key, responseError(err, http.StatusInternalServerError))
		return
	}
	defer res.Body.Close()

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		p.logger.Error("read Response body failed", zap.String("key", key), zap.Error(err))
		p.sendResponse(key, responseError(err, http.StatusInternalServerError))
		return
	}

	if res.StatusCode == http.StatusOK {
		p.logger.Debug("successfully received Response", zap.String("key", key), zap.Error(err))

		places := make(dto.Places, 0)

		err := json.Unmarshal(data, &places)
		if err != nil {
			p.logger.Error("unmarshal places failed", zap.String("key", key), zap.Error(err))
			p.sendResponse(key, responseError(err, http.StatusInternalServerError))
			return
		}

		widgets := make(dto.Widgets, 0, len(places))
		for _, place := range places {
			w, err := dto.NewWidjet(place)
			if err != nil {
				p.logger.Warn("create widget from place failed")
				continue
			}
			widgets = append(widgets, w)
		}

		data, err := json.Marshal(widgets)
		if err != nil {
			p.logger.Error("marshal widgets failed", zap.String("key", key), zap.Error(err))
			p.sendResponse(key, responseError(err, http.StatusInternalServerError))
			return
		}

		err = p.cache.Set(ctx, key, data)
		if err != nil {
			p.logger.Error("cache data failed", zap.String("key", key), zap.Error(err))
		}
		p.sendResponse(key, &Response{Code: http.StatusOK, Data: data})
	} else {
		p.logger.Error("Response error", zap.Error(err), zap.Int("status", res.StatusCode))
		var dtoErr dto.Error

		err := json.Unmarshal(data, &dtoErr)
		if err != nil {
			p.logger.Error("unmarshal error failed", zap.String("key", key), zap.Error(err))
			p.sendResponse(key, responseError(err, res.StatusCode))
			return
		}
	}

}

func (p *Processor) isClosed() bool {
	select {
	case <-p.ctx.Done():
		return true
	default:
	}

	return false
}

func (p *Processor) sendResponse(key string, res *Response) {
	p.lock.Lock()
	resps := p.responders[key]
	delete(p.responders, key)
	p.lock.Unlock()

	for _, resp := range resps {
		select {
		case <-p.ctx.Done():
			return
		case <-resp.ctx.Done():
			p.logger.Warn("send Response failed", zap.String("key", key), zap.Error(resp.ctx.Err()))
		case resp.res <- res:
		}
	}
}
