package processor

import "time"

type Config struct {
	EndPoint  string        `mapstructure:"end-point"`
	RateLimit int           `mapstructure:"rate-limit"`
	Timeout   time.Duration `mapstructure:"timeout"`
}
