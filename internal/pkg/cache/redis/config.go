package redis

import "time"

const (
	DefaultHost = "localhost"
	DefaultPort = 6379
)

type Config struct {
	Host               string        `mapstructure:"host"`
	Port               int           `mapstructure:"port"`
	Password           string        `mapstructure:"password"`
	DB                 int           `mapstructure:"db"`
	ReadTimeout        time.Duration `mapstructure:"read-timeout"`
	WriteTimeout       time.Duration `mapstructure:"write-timeout"`
	PoolSize           int           `mapstructure:"pool-size"`
	PoolTimeout        time.Duration `mapstructure:"pool-timeout"`
	IdleTimeout        time.Duration `mapstructure:"idle-timeout"`
	IdleCheckFrequency time.Duration `mapstructure:"idle-check-frequency"`
}

func (c *Config) withDefaults() (conf Config) {
	if c != nil {
		conf = *c
	}

	if len(conf.Host) == 0 {
		conf.Host = DefaultHost
	}

	if conf.Port == 0 {
		conf.Port = DefaultPort
	}

	return
}
