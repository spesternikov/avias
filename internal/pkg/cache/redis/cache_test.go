package redis

import (
	"context"
	"os"
	"strconv"
	"testing"
	"time"

	"avias/internal/pkg/cache"

	"github.com/stretchr/testify/assert"
)

func TestCacheSet(t *testing.T) {
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")

	portInt, err := strconv.Atoi(port)
	assert.NoError(t, err, "unexpected error")

	c, err := NewCache(&Config{Host: host, Port: portInt}, &cache.Config{Expiration: 30 * time.Second})
	assert.NoError(t, err, "unexpected error")

	err = c.Set(context.Background(), "key", []byte("value"))
	assert.NoError(t, err, "unexpected error")
}

func TestCacheGet(t *testing.T) {
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")

	portInt, err := strconv.Atoi(port)
	assert.NoError(t, err, "unexpected error")

	c, err := NewCache(&Config{Host: host, Port: portInt}, &cache.Config{Expiration: 30 * time.Second})
	assert.NoError(t, err, "unexpected error")

	err = c.Set(context.Background(), "key", []byte("value"))
	assert.NoError(t, err, "unexpected error")

	val, ok, err := c.Get(context.Background(), "key")
	assert.NoError(t, err, "unexpected error")

	assert.Equal(t, ok, true)
	assert.Equal(t, val, []byte("value"))

	val, ok, err = c.Get(context.Background(), "unknown")
	assert.NoError(t, err, "unexpected error")

	assert.NotEqual(t, ok, true)
}
