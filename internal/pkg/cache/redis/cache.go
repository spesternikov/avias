package redis

import (
	"context"
	"net"
	"strconv"

	"avias/internal/pkg/cache"

	"github.com/go-redis/redis"
	"github.com/pkg/errors"
)

type Cache struct {
	client *redis.Client

	commonConfig *cache.Config
}

func NewCache(config *Config, commonConfig *cache.Config) (*Cache, error) {
	c := config.withDefaults()

	client := redis.NewClient(&redis.Options{
		Addr:               net.JoinHostPort(c.Host, strconv.Itoa(c.Port)),
		Password:           c.Password,
		DB:                 c.DB,
		ReadTimeout:        c.ReadTimeout,
		WriteTimeout:       c.WriteTimeout,
		PoolSize:           c.PoolSize,
		PoolTimeout:        c.PoolTimeout,
		IdleTimeout:        c.IdleTimeout,
		IdleCheckFrequency: c.IdleCheckFrequency,
	})

	_, err := client.Ping().Result()
	if err != nil {
		return nil, errors.Wrap(err, "redis client ping failed")
	}

	return &Cache{
		client:       client,
		commonConfig: commonConfig,
	}, nil
}

func (c *Cache) Close() error {
	err := c.client.Close()
	if err != nil {
		return errors.Wrap(err, "redis close failed")
	}

	return nil
}

func (c *Cache) Get(ctx context.Context, key string) ([]byte, bool, error) {
	data, err := c.client.WithContext(ctx).Get(key).Result()
	if err != nil && err != redis.Nil {
		return nil, false, errors.Wrap(err, "redis get failed")
	} else if err == redis.Nil {
		return nil, false, nil
	}

	return []byte(data), true, nil
}

func (c *Cache) Set(ctx context.Context, key string, data []byte) error {
	_, err := c.client.WithContext(ctx).Set(key, string(data), c.commonConfig.Expiration).Result()
	if err != nil {
		return errors.Wrap(err, "redis set failed")
	}

	return nil
}
