package cache

import "time"

type Config struct {
	Expiration time.Duration `mapstructure:"expiration"`
}
