package memory

import (
	"context"
	"time"

	"avias/internal/pkg/cache"

	gocache "github.com/patrickmn/go-cache"
)

type Cache struct {
	cache      *gocache.Cache
	expiration time.Duration
}

func NewCache(config *Config, commonConfig *cache.Config) *Cache {
	c := config.withDefaults()

	return &Cache{
		cache:      gocache.New(commonConfig.Expiration, c.CleanupInterval),
		expiration: commonConfig.Expiration,
	}
}

func (c *Cache) Get(ctx context.Context, key string) ([]byte, bool, error) {
	val, ok := c.cache.Get(key)
	if !ok {
		return nil, false, nil
	}

	return val.([]byte), true, nil
}

func (c *Cache) Set(ctx context.Context, key string, data []byte) error {
	c.cache.Set(key, data, c.expiration)

	return nil
}

func (c *Cache) Close() error {
	return nil
}
