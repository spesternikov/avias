package memory

import "time"

const (
	DefaultCleanupInterval = 5 * time.Minute
)

type Config struct {
	CleanupInterval time.Duration `mapstructure:"cleanup-interval"`
}

func (c *Config) withDefaults() (conf Config) {
	if c != nil {
		conf = *c
	}

	if conf.CleanupInterval == 0 {
		conf.CleanupInterval = DefaultCleanupInterval
	}

	return
}
