package memory

import (
	"context"
	"testing"
	"time"

	"avias/internal/pkg/cache"

	"github.com/stretchr/testify/assert"
)

func TestCacheSet(t *testing.T) {
	c := NewCache(&Config{CleanupInterval: time.Minute}, &cache.Config{Expiration: 30 * time.Second})

	err := c.Set(context.Background(), "key", []byte("value"))
	assert.NoError(t, err, "unexpected error")
}

func TestCacheGet(t *testing.T) {
	c := NewCache(&Config{CleanupInterval: time.Minute}, &cache.Config{Expiration: 30 * time.Second})

	err := c.Set(context.Background(), "key", []byte("value"))
	assert.NoError(t, err, "unexpected error")

	val, ok, err := c.Get(context.Background(), "key")
	assert.NoError(t, err, "unexpected error")

	assert.Equal(t, ok, true)
	assert.Equal(t, val, []byte("value"))

	val, ok, err = c.Get(context.Background(), "unknown")
	assert.NoError(t, err, "unexpected error")

	assert.NotEqual(t, ok, true)
}
