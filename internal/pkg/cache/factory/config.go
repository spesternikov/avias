package factory

import (
	"avias/internal/pkg/cache"
	"avias/internal/pkg/cache/memory"
	"avias/internal/pkg/cache/redis"
)

type Config struct {
	Type   string         `mapstructure:"type"`
	Cache  cache.Config   `mapstructure:"common"`
	Redis  *redis.Config  `mapstructure:"redis"`
	Memory *memory.Config `mapstructure:"memory"`
}
