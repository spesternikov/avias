package factory

import (
	"avias/internal/pkg/cache"
	"avias/internal/pkg/cache/memory"
	"avias/internal/pkg/cache/redis"

	"github.com/pkg/errors"
)

const (
	TypeMemory = "memory"
	TypeRedis  = "redis"
)

func NewCache(config *Config) (cache.Cache, error) {
	switch config.Type {
	case TypeMemory:
		return memory.NewCache(config.Memory, &config.Cache), nil
	case TypeRedis:
		return redis.NewCache(config.Redis, &config.Cache)
	}

	return nil, errors.Errorf("unknown cache type: %s", config.Type)
}
