NAME= avias
TARGET=registry.gitlab.com/spesternikov/avias
CONFIG_FILE=example.yaml
GOPATH=$(shell go env GOPATH)
CURRENT_DIR = $(shell pwd)

# go source files, ignore vendor directory
SRC = $(shell find . -type f -name '*.go' -not -path "./vendor/*" -not -path "./service/*")

default: all-gen deps build

.PHONY: clean
clean:
	rm -rf bin/*
	rm -rf vendor/*

.PHONY: easyjson
easyjson:
	go get -u github.com/mailru/easyjson/...

.PHONY: mod
mod:
    GO111MODULE=on go mod vendor

.PHONY: fmt
fmt:
	@gofmt -l -w $(SRC)

.PHONY: simplify
simplify:
	@gofmt -s -l -w $(SRC)

.PHONY: build
build: fmt
	GO111MODULE=on go build -o ./bin/$(NAME) ./main.go

go-json-gen:
	rm pkg/dto/*_easyjson.go
	easyjson -all pkg/dto/*

.PHONY: dock
test:
	REDIS_HOST=${REDIS_HOST} REDIS_PORT=${REDIS_PORT} go test -v ./internal/pkg/cache/redis ./internal/pkg/cache/memory

.PHONY: docker-build
docker-build:
	docker build -t $(TARGET) .

.PHONY: docker-compose
docker-compose:
	docker-compose up -d

