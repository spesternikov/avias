package dto

import "github.com/pkg/errors"

//easyjson:json
type Widget struct {
	Slug     string `json:"slug"`
	Subtitle string `json:"subtitle"`
	Title    string `json:"title"`
}

//easyjson:json
type Widgets []*Widget

func NewWidjet(p *Place) (*Widget, error) {
	switch p.Type {
	case TypeAirpoty:
		return &Widget{
			Slug:     p.Code,
			Subtitle: p.CityName,
			Title:    p.Name,
		}, nil

	case TypeCity:
		return &Widget{
			Slug:     p.Code,
			Subtitle: p.CountryName,
			Title:    p.Name,
		}, nil
	}

	return nil, errors.Errorf("unknown place type: %s", p.Type)
}
