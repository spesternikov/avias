package dto

const (
	TypeAirpoty = "airport"
	TypeCity    = "city"
)

//easyjson:json
type Place struct {
	CityName    string `json:"city_name"`
	CountryName string `json:"country_name"`
	Code        string `json:"code"`
	Name        string `json:"name"`
	Type        string `json:"type"`
}

//easyjson:json
type Places []*Place
