package server

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"avias/internal/pkg/cache"
	"avias/internal/pkg/logger"
	"avias/internal/pkg/processor"
	"avias/internal/pkg/server"

	cacheFactory "avias/internal/pkg/cache/factory"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/dig"
	"go.uber.org/zap"
)

type Config struct {
	Logger    logger.Config       `mapstructure:"logger"`
	Cache     cacheFactory.Config `mapstructure:"cache"`
	Processor processor.Config    `mapstructure:"processor"`
	Server    server.Config       `mapstructure:"server"`
}

var Cmd = &cobra.Command{
	Use:   "server",
	Short: "Start http server",
	Run: func(cmd *cobra.Command, args []string) {
		cfg := &Config{}

		if err := viper.Unmarshal(cfg); err != nil {
			fmt.Printf("Failed to parse config: %v\n", err)
			os.Exit(1)
		}
		log, err := logger.New(&cfg.Logger)
		if err != nil {
			fmt.Printf("Failed to instantiate logger: %v\n", err)
			os.Exit(1)
		}
		log.Sugar().Infof("Starting with config: %+v", cfg)

		container := buildContainer(cfg, log)

		err = container.Invoke(func(s *server.Server, c cache.Cache, p *processor.Processor) {
			ctx, cancel := context.WithCancel(context.Background())

			stopChan := make(chan os.Signal, 1)
			signal.Notify(stopChan, syscall.SIGTERM, syscall.SIGQUIT)

			go func() {
				c := <-stopChan
				log.Warn("Caught signal", zap.String("signal", c.String()))
				cancel()
			}()

			go func() {
				err := s.Serve()
				if err != nil {
					log.Fatal("server serve failed")
				}
			}()

			<-ctx.Done()

			s.Shutdown()
			c.Close()
			p.Close()
		})
		if err != nil {
			log.Fatal("invoke container failed", zap.Error(err))
		}
	},
}

func buildContainer(config *Config, logger *zap.Logger) *dig.Container {
	container := dig.New()

	err := container.Provide(func() *zap.Logger {
		return logger
	})
	if err != nil {
		logger.Fatal("container provide failed", zap.Error(err))
	}

	err = container.Provide(func() *cacheFactory.Config {
		return &config.Cache
	})
	if err != nil {
		logger.Fatal("container provide failed", zap.Error(err))
	}

	err = container.Provide(cacheFactory.NewCache)
	if err != nil {
		logger.Fatal("container provide failed", zap.Error(err))
	}

	err = container.Provide(func() *processor.Config {
		return &config.Processor
	})
	if err != nil {
		logger.Fatal("container provide failed", zap.Error(err))
	}

	err = container.Provide(processor.New)
	if err != nil {
		logger.Fatal("container provide failed", zap.Error(err))
	}

	err = container.Provide(func() *server.Config {
		return &config.Server
	})
	if err != nil {
		logger.Fatal("container provide failed", zap.Error(err))
	}

	err = container.Provide(server.New)
	if err != nil {
		logger.Fatal("container provide failed", zap.Error(err))
	}

	return container
}
