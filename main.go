package main

import (
	"avias/cmd"
	"avias/cmd/server"
)

func main() {
	cmd.RootCmd.AddCommand(server.Cmd)

	cmd.Execute()
}
